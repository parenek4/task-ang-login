import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular app';

  onLoginAttempted(loginResult: any): void {
    console.log(loginResult);
    if (loginResult.isLoggedIn) {

    }else {
      alert(loginResult.message)
    }
    
  }
}
